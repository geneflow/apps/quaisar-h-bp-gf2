QuAISAR-H GeneFlow2 App
=======================

Version: 1.0.9-01

This GeneFlow app wraps the QuAISAR-H pipeline.

Inputs
------

1. input: Input Directory - directory that contains reads.

Parameters
----------

1. input_format: 1:_SX_L001_RX_00X.fastq.gz 2: _(R)X.fastq.gz 3: _RX_00X.fastq.gz 4: _SX_RX_00X.fastq.gz. Default: 1

2. db_dir: location of databases: Default: /scicomp/reference/QuAISAR_DBs

3. output: Output project name. Default: project
